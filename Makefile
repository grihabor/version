VIRTUALENV               := virtualenv
PYTHON3.6                := python3.6
PIP                      := pip
PYTEST                   := pytest -o cache_dir=.pytest_cache --showlocals
BLACK                    := black

venv:
	$(VIRTUALENV) -p $(PYTHON3.6) venv

.PHONY: develop
develop:
	$(PIP) install pytest

test:
	$(PYTEST) tests/

format:
	$(BLACK) tests/

import subprocess


def test_readme():
    with open('README.md') as readme:
        for line in readme:
            for word in line.split():
                if not word.startswith('http'):
                    continue

                parts = word.split('/')
                tags = subprocess.check_output(['git', 'tag', '--sort=committerdate']).decode('utf-8').strip()
                last_tag = tags.rsplit('\n', 1)[-1].strip()
                assert last_tag in parts

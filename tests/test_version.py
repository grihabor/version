import os
import subprocess
import shutil
import uuid
from datetime import datetime
from pathlib import Path
from typing import Callable

import pytest


def git_factory(cwd: str) -> Callable:
    def git(*args):
        return subprocess.check_output(("git",) + args, cwd=cwd).decode("utf-8")

    return git


@pytest.fixture
def git_repo(tmpdir):
    repo = Path(tmpdir) / "repo"
    os.mkdir(repo)
    makefile = repo / "Makefile.version"
    shutil.copyfile("Makefile.version", makefile)
    git = git_factory(repo)
    git("init")
    git("config", "user.name", "pytest")
    git("config", "user.email", "pytest@gmail.com")
    git("add", makefile.name)
    commit_message = "Add Makefile.version"
    git("commit", "-m", commit_message)
    git("tag", "v1.2.3")
    make = make_factory(makefile)
    return git, repo, makefile, make


def make_factory(makefile: Path) -> Callable:
    def make(*args):
        return subprocess.check_output(("make", "-f", makefile, "-s") + args).decode(
            "utf-8"
        )

    return make


@pytest.mark.parametrize(
    "target", ["version", "version-micro", "version-minor", "version-major"]
)
def test_make_version_on_tag(git_repo, target):
    git, repo, makefile, make = git_repo
    assert "1.2.3" == make(target)


def git_commit(git, repo):
    code_file = repo / str(uuid.uuid4())
    with open(code_file, "w"):
        pass

    git("add", code_file)
    git("commit", "-m", f"Add {code_file}")


@pytest.mark.parametrize("commit_count", [1, 2, 3])
@pytest.mark.parametrize(
    "target, version",
    [
        ("version", "1.2.4.dev{count}+{commit}"),
        ("version-micro", "1.2.4"),
        ("version-minor", "1.3.0"),
        ("version-major", "2.0.0"),
    ],
)
def test_make_version(git_repo, target, version, commit_count):
    git, repo, makefile, make = git_repo
    for i in range(commit_count):
        git_commit(git, repo)
    commit_sha = git("rev-parse", "HEAD")
    assert version.format(
        count=commit_count,
        commit=commit_sha[:8],
    ) == make(target)


@pytest.mark.parametrize("target", ["tag-micro", "tag-minor", "tag-major"])
def test_make_tag_on_tag(git_repo, target):
    git, repo, makefile, make = git_repo
    assert "v1.2.3" == make(target)


@pytest.mark.parametrize("commit_count", [1, 2, 3])
@pytest.mark.parametrize(
    "target, tag",
    [("tag-micro", "v1.2.4"), ("tag-minor", "v1.3.0"), ("tag-major", "v2.0.0")],
)
def test_make_tag_on_tag(git_repo, target, tag, commit_count):
    git, repo, makefile, make = git_repo
    for _ in range(commit_count):
        git_commit(git, repo)
    assert tag == make(target)


def test_timestamp(git_repo):
    git, repo, makefile, make = git_repo
    timestamp = float(datetime.now().timestamp())
    assert abs(int(make('timestamp')) - timestamp) < 1

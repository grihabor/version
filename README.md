# Makefile for Semantic Versioning

## Dependencies

You will need `make` and `git` installed

## Install via wget

```bash
wget -O Makefile https://gitlab.com/grihabor/version/raw/v1.3.0/Makefile.version
```

## Install via curl

```bash
curl -o Makefile https://gitlab.com/grihabor/version/raw/v1.3.0/Makefile.version
```

## Usage

First, you need to create a git tag prefixed with `v` for targets to work:

```bash
git tag v0.0.1
```

Then you can generate version for subsequent commits:

```bash
make version
```

See more targets and usage examples in [Makefile.version](Makefile.version) and [tests/](tests)
